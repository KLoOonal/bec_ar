﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Runtime.InteropServices;

public class GameManagerController : MonoBehaviour {

	[Header("native variable")]
	public string bundle = "com.BECi.live3";

	[Header ("Preload Data")]
	public string verified;
	public int result;
	public string box1Url;
	public string box2Url;
	public string box3Url;
	public string daraUrl;
	public string instructionUrl;

	[Header ("Preload Object")]
	public Image imageBox1;
	public Image  imageBox2;
	public Image  imageBox3;
	public Image imageLeft;
	public Image imageMid;
	public Image imageRight;
	public Image  imageDara;

	[Header ("Global Stage")]
	public bool ready = true;
	public bool firstStage = false;
	public bool secondStage = false;

	[Header ("Phase 1 Variable")]

	public GameObject firstGUI;
	public GameObject originalPanel;
	public GameObject animationPoint;


	[Header ("Phase 2 Variable")]

	public GameObject secondGUI;
	public GameObject animationPointTwo;
	public bool impact = false;

	[Header ("Phase 3 Variable")]

	public GameObject thirdGUI;
	public GameObject animationPointThree;
	//
	private int numGUI = 0;
	public float impactDelay = 0.4f;
	private float impactCurrent = 0.0f;
	public int step = 1;


	// Use this for initialization
	IEnumerator Start () {

		WWW box1 = new WWW(box1Url);
		yield return box1;
		WWW box2 = new WWW (box2Url);
		yield return box2;
		WWW box3 = new WWW (box3Url);
		yield return box3;
		WWW dara = new WWW (daraUrl);
		yield return dara;






		// box 1 left

		imageBox1.sprite = Sprite.Create(box1.texture, new Rect(0, 0, box1.texture.width, box1.texture.height), new Vector2(0, 0));
		imageLeft.sprite = Sprite.Create(box1.texture, new Rect(0, 0, box1.texture.width, box1.texture.height), new Vector2(0, 0));
		// box 2 mid
		imageBox2.sprite = Sprite.Create(box2.texture, new Rect(0, 0, box2.texture.width, box2.texture.height), new Vector2(0, 0));
		imageMid.sprite =  Sprite.Create(box2.texture, new Rect(0, 0, box2.texture.width, box2.texture.height), new Vector2(0, 0));
		// box 3 right
		imageBox3.sprite = Sprite.Create(box3.texture, new Rect(0, 0, box3.texture.width, box3.texture.height), new Vector2(0, 0));
		imageRight.sprite = Sprite.Create(box3.texture, new Rect(0, 0, box3.texture.width, box3.texture.height), new Vector2(0, 0));

		// Dara pic

		imageDara.sprite = Sprite.Create(dara.texture, new Rect(0, 0, dara.texture.width, dara.texture.height), new Vector2(0, 0));
		Debug.Log (box2);
		

		ready = true;
		yield return null;
	}
	
	// Update is called once per frame
	void Update () {
		if(ready==true){
			stageOne ();
			stageTwo ();

		}
	}

	// Stage 1
	void stageOne(){
		if(firstStage == false && step == 1){
			firstStage = true;
			animationPoint.GetComponent<Animator> ().SetTrigger ("probintro");
		}

	}
	// Stage 2
	void stageTwo(){
		if(secondStage == false && step == 2){
			secondStage = true;
			animationPointTwo.GetComponent<Animator> ().SetTrigger ("buttonintro");
			impact = true;
		}
		doImpact ();
	}
		

	/// //////////////////////////////////////////////////


	// Native receiver method
	public void unityVerify(int result , string veriString , string gift1 , string gift2 , string gift3 ,string daRa){
		verified = veriString;
		this.result = result;
	}


	// Native sender method
	#if UNITY_IPHONE

	#endif

	public void unityGetStop(){
		#if UNITY_IPHONE
		// iphone method
		// iphone application quit
		Application.Quit();

		#endif


		#if UNITY_ANDROID
		// android bundle name 
		// android method  
		// android application quit
		using (AndroidJavaObject sendObject = new AndroidJavaObject (bundle))
		{  
			//sendObject.Call ("stopSelf");
			Application.Quit();
		}  
		#endif
	}

	/// ////////////////////////////////////////////////////////////////////


	// GET and SET Method
	public void getItClicked(){
		if(originalPanel.activeSelf == true){
			originalPanel.SetActive (false);
			animationPoint.GetComponent<Animator> ().SetTrigger ("probout");
			numGUI = 1;
			Invoke ("closeGUI", 1.0f);
		}

	}


	public bool confirmAR(){

		// verify AR image if it valid return true then false.
		// this testing case is always true.
		if(numGUI == 1){
		step = 2;
		return true;
		}else {
			return false;
		}
	}

	public void getChoose(int i){
		// clear stage 2
		numGUI = 2;
		impact = false;
		closeGUI ();
		step = 3;
		thirdGUI.SetActive (true);
		//
		if(i==1){
			animationPointThree.GetComponent<Animator> ().SetTrigger ("imageleft");
		}else if(i==2){
			animationPointThree.GetComponent<Animator> ().SetTrigger ("imagemid");
		}else if(i==3){
			animationPointThree.GetComponent<Animator> ().SetTrigger ("imageright");
		}

	}


	public void clamPize(){
		if(step == 3){
			// close unity and back to application
			SceneManager.LoadScene("AR_SCENE");
		}
	}

	// Private method
	private void closeGUI(){
		if(numGUI ==1){
			firstGUI.SetActive (false);
		}else if(numGUI ==2){
			secondGUI.SetActive (false);
		}else if(numGUI ==3){
			thirdGUI.SetActive (false);
		}
	}

	private void doImpact(){

		if(impact == true){
		impactCurrent += Time.deltaTime;
		}
		if(impact == true && impactCurrent >= impactDelay){
			if(Input.acceleration.magnitude > 1.3f){
				impactCurrent = 0.0f;
			animationPointTwo.GetComponent<Animator> ().SetTrigger ("buttonaction");
			}
		}
	}
}
